<?php

class Goal
{
	public $actualFlag = false; // sets the flag of actual mark if this flag is true then method workoutEnergySpending() will return only items with actual field == 1
	
	private $ABS;
	private $INTENSITY_LABOR_FACTORS;
	private $PROFILE;
	private $plannedDate;
	
	private $bodyMassIndex;
	private $valueOfBasalMetabolism;
	private $standardEnergySpending;
	private $calorieGoal;
	private $minCalorieBoundry;
	
	public function Goal( $plannedDate = 0, $actualFlag = false, $rationID = 0, $unixTimeStamp = '' )
	{
		global $user, $db, $prf;
		
		if( !empty( $plannedDate ) )
		{
			$this->plannedDate = $plannedDate;
		}
		else 
		{
			$this->plannedDate = date( 'd.m.Y' );
		}
		
		$this->actualFlag = $actualFlag;
		
		$this->INTENSITY_LABOR_FACTORS = array( 'type_1' => 1.3,
													'type_2' => 1.4,
													'type_3' => 1.6,
													'type_4' => 1.9,
													'type_5' => 2.2,
													'type_6' => 2.5
												);
		
		$this->PROFILE = $user->profile( $unixTimeStamp );
		
		if( !empty( $rationID ) )
		{
			$_RATION = $db->first( ' SELECT `weight`, `regimen`, `regimen_type` FROM `' . $prf . 'ration` WHERE `id` = "' . intval( $rationID ) . '" LIMIT 1 ' );
			
			$this->PROFILE['weight'] = $_RATION['weight'];
			$this->PROFILE['regimen'] = $_RATION['regimen'];
			$this->PROFILE['regimen_type'] = $_RATION['regimen_type'];
		}
		
		// defina a user group and relace gender with it
		if( $this->PROFILE['gender'] != 'female-pregnant-second-part' && $this->PROFILE['gender'] != 'female-feeding-1-6' && $this->PROFILE['gender'] != 'female-feeding-7-12' )
		{
			if( $this->PROFILE['age'] == 1 )
			{
				$this->PROFILE['gender'] = 'child-1';
			}
			else if( $this->PROFILE['age'] == 2 )
			{
				$this->PROFILE['gender'] = 'child-2';
			}
			else if( $this->PROFILE['age'] >= 3 && $this->PROFILE['age'] <= 7 )
			{
				$this->PROFILE['gender'] = 'child-3-7';
			}
			else if( $this->PROFILE['age'] >= 8 && $this->PROFILE['age'] <= 11 )
			{
				$this->PROFILE['gender'] = 'child-7-11';
			}
			
			if( $this->PROFILE['gender'] == 'female' )
			{
				if( $this->PROFILE['age'] >= 12 && $this->PROFILE['age'] <= 14 )
				{
					$this->PROFILE['gender'] = 'female-11-14';
				}
				else if( $this->PROFILE['age'] >= 15 && $this->PROFILE['age'] <= 18 )
				{
					$this->PROFILE['gender'] = 'female-14-18';
				}
			}
			else if( $this->PROFILE['gender'] == 'male' )
			{
				if( $this->PROFILE['age'] >= 12 && $this->PROFILE['age'] <= 14 )
				{
					$this->PROFILE['gender'] = 'male-11-14';
				}
				else if( $this->PROFILE['age'] >= 15 && $this->PROFILE['age'] <= 18 )
				{
					$this->PROFILE['gender'] = 'male-14-18';
				}
			}
		}
		
		// define callorie goal
		$this->bodyMassIndex = $this->bodyMassIndex();
		
		$this->valueOfBasalMetabolism = $this->valueOfBasalMetabolism();
		$_valueOfBasalMetabolism = $this->valueOfBasalMetabolism;
		
		$this->standardEnergySpending = $this->standardEnergySpending();
		
		$this->calorieGoal = $this->calorieGoal();
		$proteinGoal = $this->proteinGoal();
		$fatGoal = $this->fatGoal();
		$waterGoal = $this->waterGoal();
		
		$WORKOUT_ENERGY_SPENDING = $this->workoutEnergySpending();
		
		if( ( $this->calorieGoal - $WORKOUT_ENERGY_SPENDING['counted'] >= $_valueOfBasalMetabolism ) && $WORKOUT_ENERGY_SPENDING['counted'] > 0 )
		{
			$minCalorieBoundry = $this->calorieGoal;
		}
		else if( $this->calorieGoal - $WORKOUT_ENERGY_SPENDING['counted'] < $_valueOfBasalMetabolism )
		{
			$beta = $_valueOfBasalMetabolism - $this->calorieGoal + $WORKOUT_ENERGY_SPENDING['counted'];
			$minCalorieBoundry = $beta + $this->calorieGoal;
			
			$_valueOfBasalMetabolism += $beta;
		}
		else 
		{
			$minCalorieBoundry = 0;
		}
		
		$this->calorieGoal += $WORKOUT_ENERGY_SPENDING['counted'];
		
		if( $this->PROFILE['regimen'] == 'weight_gain' )
		{
			$minCalorieBoundry = 0;
			$_valueOfBasalMetabolism = $this->standardEnergySpending + $WORKOUT_ENERGY_SPENDING['counted'];
		}
		else if( $this->PROFILE['regimen'] == 'harmony' )
		{
			$minCalorieBoundry = 0;
		}
		
		$this->minCalorieBoundry = $minCalorieBoundry;
		
		$CARBO_GOAL = $this->carboGoal();
		
		$this->ABS = array(
							'standard_energy_spending' => $this->standardEnergySpending,
							'workout_energy_spending' => $WORKOUT_ENERGY_SPENDING['real'],
							
							'value_of_basal_metabolism_percent' => round( Misc::getPercentage( $_valueOfBasalMetabolism, $this->calorieGoal ) ),
							'value_of_basal_metabolism_abs' => $_valueOfBasalMetabolism,
		
							'min_calorie_boundry_percent' => round( Misc::getPercentage( $minCalorieBoundry, $this->calorieGoal ) ),
							'min_calorie_boundry_abs' => $minCalorieBoundry,
							
							'calorie' => $this->calorieGoal,
							'protein' => $proteinGoal,
							'fat' => $fatGoal,
							'carbo' => $CARBO_GOAL['carbo'],
		
							'min_carbo_boundry_percent' => round( Misc::getPercentage( $CARBO_GOAL['min_carbo_boundry_abs'], $CARBO_GOAL['carbo'] ) ),
							'min_carbo_boundry_abs' => $CARBO_GOAL['min_carbo_boundry_abs'],
		
							'water' => $waterGoal,
							'ration_water' => $waterGoal,
							'total_water' => $waterGoal,
								
								'fiber' => 0,
								
								'vitamin_a_rae' => 0,
								'beta_carotene' => 0,
								'thiamin' => 0,
								'riboflavin' => 0,
								
								'choline' => 0,
								'pantothenic_acid' => 0,
								'pyridoxine' => 0,
								'folic_acid' => 0,
								'cyanocobalamin' => 0,
								
								'ascorbic_acid' => 0,
								'cholecalciferol' => 0,
								'alpha_tocopherol' => 0,
								'biotin' => 0,
								'phylloquinone' => 0,
								
								'niacin' => 0,
								'calcium' => 0,
								'phosphorus' => 0,
								'magnesium' => 0,
								'potassium' => 0,
								
								'sodium' => 0,
								'chlorine' => 0,
								'silicon' => 0,
								'iron' => 0,
								'zinc' => 0,
								
								'iodine' => 0,
								'copper' => 0,
								'manganese' => 0,
								'selenium' => 0,
								'chromium' => 0,
								
								'molybdenum' => 0,
								'fluorine' => 0,
								'cobalt' => 0,
								'cholesterol' => 0,
		);
		
		// nutrient goals from table
		$GOALS = $db->qFetch( ' SELECT `goal_value`, `code` FROM `' . $prf . 'nutrient_goal` WHERE `user_group` = "' . $db->mres( $this->PROFILE['gender'] ) . '" ' );
		
		//
		foreach( $GOALS as $GOAL )
		{
			if( isset( $this->ABS[ $GOAL['code'] ] ) )
			{
				$this->ABS[ $GOAL['code'] ] = $GOAL['goal_value'];
			}
		}
		
		// increase valaues Calorie, Protein, Fat and Carbo for pregnant and feeding women
		if( $this->PROFILE['gender'] == 'female-pregnant-second-part' )
		{
			$this->ABS['calorie'] += 350;
			$this->ABS['protein'] += 30;
			$this->ABS['fat'] += 12;
			$this->ABS['carbo'] += 30;
			
			$this->ABS['standard_energy_spending'] += 350;
		}
		else if( $this->PROFILE['gender'] == 'female-feeding-1-6' )
		{
			$this->ABS['calorie'] += 500;
			$this->ABS['protein'] += 40;
			$this->ABS['fat'] += 15;
			$this->ABS['carbo'] += 40;
			
			$this->ABS['standard_energy_spending'] += 500;
		}
		else if( $this->PROFILE['gender'] == 'female-feeding-7-12' )
		{
			$this->ABS['calorie'] += 450;
			$this->ABS['protein'] += 30;
			$this->ABS['fat'] += 15;
			$this->ABS['carbo'] += 30;
			
			$this->ABS['standard_energy_spending'] += 450;
		}
		
		if( $this->PROFILE['gender'] ==  'child-1' || 
			$this->PROFILE['gender'] ==  'child-2' || 
			$this->PROFILE['gender'] ==  'child-3-7' || 
			$this->PROFILE['gender'] ==  'child-7-11' || 
			$this->PROFILE['gender'] ==  'male-11-14' || 
			$this->PROFILE['gender'] ==  'female-11-14' || 
			$this->PROFILE['gender'] ==  'male-14-18' || 
			$this->PROFILE['gender'] ==  'female-14-18' )
		{
			$this->ABS['standard_energy_spending'] = $this->ABS['calorie'];
		}
		
		if( $this->PROFILE['far_north'] == '1' )
		{
			foreach( $this->ABS as $key => $value )
			{
				if( $key == 'calorie' || $key == 'protein' || $key == 'fat' || $key == 'carbo' )
				{
					$this->ABS[$key] += Misc::getPercent( 15, $value );
				}
			}
		}
		
		foreach( $this->ABS as $key => $value )
		{
			$this->ABS[$key] = Ration::round( $value, $key );
		}
	}
	
	// return the body mass index for particular user according to his profile
	public function bodyMassIndex()
	{
		if( !empty( $this->PROFILE['weight'] ) && !empty( $this->PROFILE['height'] ) )
		{
			$bodyMassIndex = $this->PROFILE['weight'] / pow( ( $this->PROFILE['height'] / 100 ), 2 );
		}
		else 
		{
			$bodyMassIndex = 0;
		}
		
		return round( $bodyMassIndex, 2 );
	}
	
	public function valueOfBasalMetabolism()
	{
		if( $this->PROFILE['regimen'] == 'weight_loss' && $this->bodyMassIndex > 25 )
		{
			$weight = 25 * pow( ( $this->PROFILE['height'] / 100 ), 2 );
		}
		else 
		{
			$weight = $this->PROFILE['weight'];
		}
		
		if( $this->PROFILE['gender'] == 'male' )
		{
			// Ð’ÐžÐž = 9,99 Ñ… Ð²ÐµÑ (ÐºÐ³) + 6.25 Ñ… Ñ€Ð¾ÑÑ‚ (Ð² ÑÐ°Ð½Ñ‚Ð¸Ð¼ÐµÑ‚Ñ€Ð°Ñ…) - 4,92 Ñ… Ð²Ð¾Ð·Ñ€Ð°ÑÑ‚ + 5 
			
			$valueOfBasalMetabolism = 9.99 * $weight + 6.25 * $this->PROFILE['height'] - 4.92 * $this->PROFILE['age'] + 5;
		}
		else if( preg_match( '/female/i', $this->PROFILE['gender'] ) ) // $this->PROFILE['gender'] == 'female'
		{
			// Ð’ÐžÐž = 9,99 Ñ… Ð²ÐµÑ (ÐºÐ³) + 6.25 Ñ… Ñ€Ð¾ÑÑ‚ (Ð² ÑÐ°Ð½Ñ‚Ð¸Ð¼ÐµÑ‚Ñ€Ð°Ñ…) - 5 Ñ… Ð²Ð¾Ð·Ñ€Ð°ÑÑ‚ - 161
			
			$valueOfBasalMetabolism = 9.99 * $weight + 6.25 * $this->PROFILE['height'] - 5 * $this->PROFILE['age'] - 161;
		}
		else 
		{
			$valueOfBasalMetabolism = 0;
		}
		
		return round( $valueOfBasalMetabolism );
	}
	
	public function standardEnergySpending()
	{
		if( isset( $this->INTENSITY_LABOR_FACTORS[ $this->PROFILE['labour_type'] ] ) )
		{
			$standardEnergySpending = $this->valueOfBasalMetabolism * $this->INTENSITY_LABOR_FACTORS[ $this->PROFILE['labour_type'] ];
		}
		else 
		{
			$standardEnergySpending = 0;
		}
		
		return round( $standardEnergySpending );
	}
	
	public function calorieGoal()
	{
		if( $this->PROFILE['regimen'] == 'weight_gain' )
		{
			$percent = 25;
		}
		else if( $this->PROFILE['regimen'] == 'harmony' )
		{
			$percent = 0;
		}
		else if( $this->PROFILE['regimen'] == 'weight_loss' )
		{
			if( $this->PROFILE['regimen_type'] == 'hard' )
			{
				$percent = 23;
			}
			else if( $this->PROFILE['regimen_type'] == 'moderate' )
			{
				$percent = 17;
			}
			else if( $this->PROFILE['regimen_type'] == 'easy' )
			{
				$percent = 14;
			}
		}
		
		
		if( isset( $percent ) )
		{
			$diff = Misc::getPercent( $percent, $this->standardEnergySpending );
			
			if( $this->PROFILE['regimen'] == 'weight_gain' )
			{
				$calorieGoal = $this->standardEnergySpending + $diff;
			}
			else if( $this->PROFILE['regimen'] == 'weight_loss' )
			{
				$calorieGoal = $this->standardEnergySpending - $diff;
			}
			else if( $this->PROFILE['regimen'] == 'harmony' )
			{
				$calorieGoal = $this->standardEnergySpending;
			}
		}
		else 
		{
			$calorieGoal = 0;
		}
		
		return round( $calorieGoal );
	}
	
	public function timeToAchieveGoal()
	{
		$calorieGoal = $this->calorieGoal();
		
		$calorieDeficit = $this->standardEnergySpending - $calorieGoal;
		
		// weight loss in gramms per day
		$grammPerDay = round( $calorieDeficit * 100 / 770 );
		$exceedWeight = $this->PROFILE['weight'] - $this->PROFILE['desired_weight'];
		
		if( $grammPerDay > 0 && $this->PROFILE['weight'] > 0 && $this->PROFILE['desired_weight'] > 0 )
		{
			// days in which the goal will be achieved
			$days = round( ( $exceedWeight * 1000 ) / $grammPerDay );
			
			$months = floor( $days / 30 );
			$days = $days % 30;
		}
		else 
		{
			$months = 0;
			$days = 0;
		}
		
		return array( 'months' => $months, 'days' => $days );
	}
	
	public function suggestedWeight()
	{
		global $db, $prf;
		
		$userGroup = $this->PROFILE['gender'];
		
		if( $userGroup == 'female-feeding-1-6' || $userGroup == 'female-feeding-7-12' )
		{
			$userGroup = 'female';
		}
		
		$RECORD = $db->qOne( ' SELECT `suggested_weight` FROM `' . $prf . 'suggested_weight` WHERE `user_group` = "' . $db->mres( $userGroup ) . '" AND `body_type` = "' . $db->mres( $this->PROFILE['body_type'] ) . '" AND `height` = "' . $db->mres( $this->PROFILE['height'] ) . '" LIMIT 1 ' );
		
		if( isset( $RECORD['suggested_weight'] ) )
		{
			$suggestedWeight = $RECORD['suggested_weight'];
		}
		else 
		{
			$suggestedWeight = '';
		}
		
		return $suggestedWeight;
	}
	
	public function workoutEnergySpending()
	{
		$workoutList = new WorkoutList();
		$workoutList->set( $this->plannedDate );
		
		$calorieTotal = 0;
		
		$workoutList->setActualFlag( $this->actualFlag );
		
		$ITEMS = $workoutList->items( $workoutListID = 0, $diary = 1 );
		
		foreach( $ITEMS as $ITEM )
		{
			$calorieTotal += $ITEM['calorie'];
		}
		
		if( $this->PROFILE['gender'] == 'male' )
		{
			$threshold1 = 400;
			$threshold2 = 700;
		}
		else if( preg_match( '/female/i', $this->PROFILE['gender'] ) )
		{
			$threshold1 = 300;
			$threshold2 = 600;
		}
		else 
		{
			$threshold1 = 0;
			$threshold2 = 0;
		}
		
		if( $calorieTotal < $threshold1 || $this->PROFILE['regimen'] == 'weight_gain' )
		{
			$countedCalorie = $calorieTotal;
		}
		else if( $calorieTotal < $threshold2 )
		{
			$countedCalorie = ( $threshold1 + ( $calorieTotal - $threshold1 ) / 2 );
		}
		else 
		{
			$countedCalorie = ( $threshold1 + ( $threshold2 - $threshold1 ) / 2 );
		}
		
		return array( 'real' => $calorieTotal, 'counted' => $countedCalorie );
	}
	
	public function proteinGoal()
	{
		if( $this->PROFILE['regimen'] == 'weight_loss' || empty( $this->PROFILE['regimen'] ) )
		{
			$proteinGoal = $this->standardEnergySpending * 0.18 / 4;
		}
		else if( $this->PROFILE['regimen'] == 'harmony' )
		{
			$proteinGoal = $this->standardEnergySpending * 0.18 / 4;
		}
		else if( $this->PROFILE['regimen'] == 'weight_gain' )
		{
			$proteinGoal = ( $this->standardEnergySpending + Misc::getPercent( 25, $this->standardEnergySpending ) ) * 0.2 / 4.1;
		}
		
		return round( $proteinGoal );
	}
	
	public function fatGoal()
	{
		if( $this->PROFILE['regimen'] == 'weight_loss' || empty( $this->PROFILE['regimen'] ) )
		{
			$fatGoal = $this->standardEnergySpending * 0.32 / 9;
		}
		else if( $this->PROFILE['regimen'] == 'harmony' )
		{
			$fatGoal = $this->standardEnergySpending * 0.32 / 9;
		}
		else if( $this->PROFILE['regimen'] == 'weight_gain' )
		{
			$fatGoal = ( $this->standardEnergySpending + Misc::getPercent( 25, $this->standardEnergySpending ) ) * 0.3 / 9.3;
		}
		
		return round( $fatGoal );
	}
	
	public function carboGoal() #!!!!!
	{
		$CARBO_GOAL = array();
		$CARBO_GOAL['min_carbo_boundry_abs'] = 0;
		
		$WORKOUT_ENERGY_SPENDING = $this->workoutEnergySpending();
		$standardEnergySpending = $this->standardEnergySpending();
		
		if( $this->PROFILE['regimen'] == 'weight_loss' || empty( $this->PROFILE['regimen'] ) )
		{
			/* 
			Ð•ÑÐ»Ð¸ Ñ‚Ñ€ÐµÐ½Ð¸Ñ€Ð¾Ð²ÐºÐ¸ Ð½Ðµ Ð±Ñ‹Ð»Ð¾
			Ð¡ÑƒÑ‚Ð¾Ñ‡Ð½Ð°Ñ Ð¿Ð¾Ñ‚Ñ€ÐµÐ±Ð½Ð¾ÑÑ‚ÑŒ ÑƒÐ³Ð»ÐµÐ²Ð¾Ð´Ð¾Ð² =( Ð ÐšÐ  â€“ ( Ð½Ð¾Ñ€Ð¼Ð° Ð±ÐµÐ»ÐºÐ¾Ð² * 4 + ÐÐ¾Ñ€Ð¼Ð° Ð¶Ð¸Ñ€Ð¾Ð² *9))/4
		
			Ð•ÑÐ»Ð¸ Ñ‚Ñ€ÐµÐ½Ð¸Ñ€Ð¾Ð²ÐºÐ° Ð±Ñ‹Ð»Ð°
			Ñ‚Ð¾ Ð½Ð¾Ñ€Ð¼Ð° ÑƒÐ³Ð»ÐµÐ²Ð¾Ð´Ð¾Ð² Ð·Ð°Ð´Ð°ÐµÑ‚ÑÑ ÐºÐ¾Ñ€Ð¸Ð´Ð¾Ñ€Ð¾Ð¼ 
			Ð¡ÑƒÑ‚Ð¾Ñ‡Ð½Ð°Ñ Ð¿Ð¾Ñ‚Ñ€ÐµÐ±Ð½Ð¾ÑÑ‚ÑŒ ÑƒÐ³Ð»ÐµÐ²Ð¾Ð´Ð¾Ð²= Ð¾Ñ‚ (Ð ÐšÐ  ÐÐ“ - (ÐÐ¾Ñ€Ð¼Ð° Ð±ÐµÐ»ÐºÐ¾Ð² * 4 + ÐÐ¾Ñ€Ð¼Ð° Ð¶Ð¸Ñ€Ð¾Ð² * 9))/4 Ð´Ð¾ (Ð ÐšÐ  Ð’Ð“ - (ÐÐ¾Ñ€Ð¼Ð° Ð±ÐµÐ»ÐºÐ¾Ð² * 4 + ÐÐ¾Ñ€Ð¼Ð° Ð¶Ð¸Ñ€Ð¾Ð² * 9))/4
			 */
			
			if( $WORKOUT_ENERGY_SPENDING['real'] > 0 ) // we don't have any workout
			{
				$CARBO_GOAL['min_carbo_boundry_abs'] = ( $this->minCalorieBoundry - ( $this->proteinGoal() * 4 + $this->fatGoal() * 9 ) ) / 4;
				$CARBO_GOAL['carbo'] = $carboGoal = ( $this->calorieGoal - ( $this->proteinGoal() * 4 + $this->fatGoal() * 9 ) ) / 4;
			}
			else 
			{
				$CARBO_GOAL['carbo'] = $carboGoal = ( $this->calorieGoal() - ( $this->proteinGoal() * 4 + $this->fatGoal() * 9 ) ) / 4;
			}
		}
		else if( $this->PROFILE['regimen'] == 'harmony' )
		{
			/*
				Ð¡ÑƒÑ‚Ð¾Ñ‡Ð½Ð°Ñ Ð¿Ð¾Ñ‚Ñ€ÐµÐ±Ð½Ð¾ÑÑ‚ÑŒ ÑƒÐ³Ð»ÐµÐ²Ð¾Ð´Ð¾Ð² = ÐÐ­ * 0,5 / 4 + Ð­Ð¢/4 Ð³
				
				Ð¡ÑƒÑ‚Ð¾Ñ‡Ð½Ð°Ñ Ð¿Ð¾Ñ‚Ñ€ÐµÐ±Ð½Ð¾ÑÑ‚ÑŒ ÑƒÐ³Ð»ÐµÐ²Ð¾Ð´Ð¾Ð² = 2200 * 0,5 / 4 + 450/4 = 388 Ð³Ñ€Ð°Ð¼Ð¼Ð¾Ð²
				ÐšÐ¾Ñ€Ð¸Ð´Ð¾Ñ€Ð¾Ð² Ð½ÐµÑ‚
			*/
			
			$CARBO_GOAL['carbo'] = $standardEnergySpending * 0.5 / 4 + $WORKOUT_ENERGY_SPENDING['real'] / 4;
		}
		else if( $this->PROFILE['regimen'] == 'weight_gain' )
		{
			/*
				Ð¡ÑƒÑ‚Ð¾Ñ‡Ð½Ð°Ñ Ð¿Ð¾Ñ‚Ñ€ÐµÐ±Ð½Ð¾ÑÑ‚ÑŒ ÑƒÐ³Ð»ÐµÐ²Ð¾Ð´Ð¾Ð² = ( ÐÐ­+25% ) * 0,5 / 4,1 + Ð­Ð¢/4,1, Ð³
				
				Ð¡ÑƒÑ‚Ð¾Ñ‡Ð½Ð°Ñ Ð¿Ð¾Ñ‚Ñ€ÐµÐ±Ð½Ð¾ÑÑ‚ÑŒ ÑƒÐ³Ð»ÐµÐ²Ð¾Ð´Ð¾Ð² = 2750 * 0,5 / 4,1 + 300 /4,1 = 408 Ð³Ñ€Ð°Ð¼Ð¼Ð¾Ð²
				ÐšÐ¾Ñ€Ð¸Ð´Ð¾Ñ€Ð¾Ð² Ð½ÐµÑ‚
			*/
			
			$CARBO_GOAL['carbo'] = ( $standardEnergySpending + Misc::getPercent( 25, $standardEnergySpending ) ) * 0.5 / 4.1 + $WORKOUT_ENERGY_SPENDING['real'] / 4.1;
		}
		
		$CARBO_GOAL['min_carbo_boundry_abs'] = round( $CARBO_GOAL['min_carbo_boundry_abs'] );
		$CARBO_GOAL['carbo'] = round( $CARBO_GOAL['carbo'] );
		
		return $CARBO_GOAL;
	}
	
	public function waterGoal()
	{
		// Ñ‚ÐµÐºÑƒÑ‰Ð¸Ð¹ Ð²ÐµÑ Ð² ÐºÐ³ Ð¥ 40 = Ð³Ñ€Ð°Ð¼Ð¼Ð¾Ð²
		
		if( $this->PROFILE['regimen'] == 'weight_loss' || empty( $this->PROFILE['regimen'] ) )
		{
			$waterGoal = $this->PROFILE['weight'] * 40;
		}
		else if( $this->PROFILE['regimen'] == 'harmony' )
		{
			$waterGoal = $this->PROFILE['weight'] * 40;
		}
		else if( $this->PROFILE['regimen'] == 'weight_gain' )
		{
			$waterGoal = $this->PROFILE['weight'] * 40;
		}
		
		return round( $waterGoal );
	}
	
	// returns absolute values of the goal
	public function abs()
	{
		return $this->ABS;
	}
	
	// returns percent values of the goal
	public function percent( $RATION_SUMMARY )
	{
		$PERCENT = array();
		
		foreach( $this->ABS as $key => $value )
		{
			if( isset( $RATION_SUMMARY[$key] ) )
			{
				$PERCENT[$key] = round( Misc::getPercentage( $RATION_SUMMARY[$key], $this->ABS[$key] ) );
			}
		}
		
		return $PERCENT;
	}
	
	public function currentWeight()
	{
		return $this->PROFILE['weight'];
	}
}

?>